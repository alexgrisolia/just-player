package jp.co.kayo.android.localplayer.adapter;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.List;

import jp.co.kayo.android.localplayer.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

public class StringListAdapter extends ArrayAdapter<String> {
    List<String> values;
    LayoutInflater inflater;

    public StringListAdapter(Context context, List<String> objects) {
        super(context, -1, objects);
        values = objects;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(R.layout.ics_simple_spinner_item, parent, false);
        }
        
        String value = values.get(position);
        TextView textview = (TextView)convertView.findViewById(android.R.id.text1);
        textview.setText(value);
        
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(R.layout.ics_simple_spinner_dropdown_item, parent, false);
        }
        
        String value = values.get(position);
        CheckedTextView textview = (CheckedTextView)convertView.findViewById(android.R.id.text1);
        textview.setText(value);
        
        return convertView;
    }
    
    

}
