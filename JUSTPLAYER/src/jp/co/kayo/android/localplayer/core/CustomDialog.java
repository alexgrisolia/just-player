package jp.co.kayo.android.localplayer.core;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

abstract public class CustomDialog extends DialogFragment implements
        OnClickListener {
    protected int layout = -1;
    protected View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.customdlg, container, false);

        if (view == null && layout != -1) {
            LayoutInflater layoutInflater = (LayoutInflater) getActivity()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(layout, null);
        }

        LinearLayout custom = (LinearLayout) root
                .findViewById(R.id.customlayout);

        custom.addView(view);

        // ボタン部
        btnOk = (Button) root.findViewById(R.id.btnOk);
        btnCancel = (Button) root.findViewById(R.id.btnCancel);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        initDialog(view);

        return root;
    }

    Button btnOk;
    Button btnCancel;

    public void enableControl(boolean b) {
        View v = view.findViewById(R.id.controlpanel);
        if (b) {
            v.setVisibility(View.VISIBLE);
        } else {
            v.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        btnOk.setEnabled(false);
        btnCancel.setEnabled(false);
        try {
            if (v.getId() == R.id.btnOk) {
                clickOK(v);
            } else if (v.getId() == R.id.btnCancel) {
                clickCancel(v);
            }
        } finally {
            btnOk.setEnabled(true);
            btnCancel.setEnabled(true);
        }
        dismiss();
    }

    abstract public void clickOK(View v);

    abstract public void initDialog(View v);

    public void clickCancel(View v) {
    }

}
