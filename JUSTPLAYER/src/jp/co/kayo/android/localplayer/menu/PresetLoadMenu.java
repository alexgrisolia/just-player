package jp.co.kayo.android.localplayer.menu;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */


import java.util.ArrayList;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;

import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.SubMenu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;

public class PresetLoadMenu extends BaseActionProvider implements OnMenuItemClickListener {
    Handler handler;

    public PresetLoadMenu(Context context) {
        super(context);
    }
    
    public void setHandler(Handler handler) {
        this.handler = handler;
    }
    
    @Override
    public void onPrepareSubMenu(SubMenu subMenu) {
        subMenu.clear();
        
        subMenu.addSubMenu(R.id.mnu_load_preset, R.id.mnu_load_preset+0, 0, context.getString(R.string.txt_preset1));
        subMenu.addSubMenu(R.id.mnu_load_preset, R.id.mnu_load_preset+1, 1, context.getString(R.string.txt_preset2));
        subMenu.addSubMenu(R.id.mnu_load_preset, R.id.mnu_load_preset+2, 2, context.getString(R.string.txt_preset3));
        subMenu.addSubMenu(R.id.mnu_load_preset, R.id.mnu_load_preset+2, 2, context.getString(R.string.txt_preset4));
        subMenu.addSubMenu(R.id.mnu_load_preset, R.id.mnu_load_preset+2, 2, context.getString(R.string.txt_preset5));
        
        for (int i = 0; i < subMenu.size(); ++i) {
            subMenu.getItem(i).setOnMenuItemClickListener(this);
        }
    }
    
    @Override
    public boolean onPerformDefaultAction() {
        if (Build.VERSION.SDK_INT < 11) {
            ArrayList<String> menuItems = new ArrayList<String>();
            menuItems.add(context.getString(R.string.txt_preset1));
            menuItems.add(context.getString(R.string.txt_preset2));
            menuItems.add(context.getString(R.string.txt_preset3));
            menuItems.add(context.getString(R.string.txt_preset4));
            menuItems.add(context.getString(R.string.txt_preset5));

            final CharSequence[] items = menuItems.toArray(new CharSequence[menuItems.size()]);
            new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.preset_btn_load))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            doItemSelect(R.id.mnu_load_preset+item, items[item].toString());
                        }
                    })
                    .show();
        }
        return super.onPerformDefaultAction();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemid = item.getItemId();
        doItemSelect(itemid, item.getTitle().toString());
        return true;
    }

    private void doItemSelect(int itemid, String item_name){
        if (item_name.startsWith(context.getString(R.string.txt_preset1))) {
            handler.sendMessage(handler.obtainMessage(SystemConsts.EVT_LOAD_PRESET, Integer.valueOf(1)));
        }
        else if (item_name.startsWith(context.getString(R.string.txt_preset2))) {
            handler.sendMessage(handler.obtainMessage(SystemConsts.EVT_LOAD_PRESET, Integer.valueOf(2)));
        }
        else if (item_name.startsWith(context.getString(R.string.txt_preset3))) {
            handler.sendMessage(handler.obtainMessage(SystemConsts.EVT_LOAD_PRESET, Integer.valueOf(3)));
        }
        else if (item_name.startsWith(context.getString(R.string.txt_preset4))) {
            handler.sendMessage(handler.obtainMessage(SystemConsts.EVT_LOAD_PRESET, Integer.valueOf(4)));
        }
        else if (item_name.startsWith(context.getString(R.string.txt_preset5))) {
            handler.sendMessage(handler.obtainMessage(SystemConsts.EVT_LOAD_PRESET, Integer.valueOf(5)));
        }
    }
}
