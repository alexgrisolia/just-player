package jp.co.kayo.android.localplayer.util.bean;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Hashtable;

import android.content.Context;

import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.util.Funcs;

public class PlaylistMemberInfo {
    public long id;
    public int track;
    public int trackTo;
    public int index;
    public long mediaId;
    public String title;
    public String album;
    public String artist;
    public long duration;
    public String data;
    public boolean loaded = false;

    public void load(Context context, String[] projection) {
        if (!loaded) {
            if (id > 0) {
                Hashtable<String, String> map = ContentsUtils.getMedia(context,
                        projection, mediaId);
                if (map != null) {
                    title = map.get(MediaConsts.AudioMedia.TITLE);
                    album = map.get(MediaConsts.AudioMedia.ALBUM);
                    artist = map.get(MediaConsts.AudioMedia.ARTIST);
                    duration = Funcs.parseLong(map
                            .get(MediaConsts.AudioMedia.DURATION));
                    data = map.get(MediaConsts.AudioMedia.DATA);
                    loaded = true;
                }
            } else {
                loaded = true;
            }
        }
    }

    public boolean isLoaded() {
        return loaded;
    }

    @Override
    public boolean equals(Object o) {
        PlaylistMemberInfo m = (PlaylistMemberInfo)o;
        return m.id == this.id;
    }
}
