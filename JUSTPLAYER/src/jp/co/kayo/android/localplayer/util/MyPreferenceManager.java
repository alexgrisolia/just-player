package jp.co.kayo.android.localplayer.util;

import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.DeviceContentProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.preference.PreferenceManager;

public class MyPreferenceManager {
    private Context context;
    private SharedPreferences defaultPref;
    private Editor editor;

    public MyPreferenceManager(Context context) {
        this.context = context;
    }

    public SharedPreferences getDefaultPref() {
        if (defaultPref == null) {
            defaultPref = PreferenceManager
                    .getDefaultSharedPreferences(context);
        }
        return defaultPref;
    }

    public Editor getEditor() {
        if (editor == null) {
            editor = getDefaultPref().edit();
        }
        return editor;
    }

    public void commit() {
        if (editor != null) {
            editor.commit();
            editor = null;
        }
    }

    public int getTabCount() {
        return Funcs.getInt(getDefaultPref().getString("key.tabcount", "4"));
    }

    public boolean isFirstBoot() {
        return getDefaultPref().getBoolean("key.firstboot", true);
    }

    public void setFirstBoot(boolean value) {
        getEditor().putBoolean("key.firstboot", value);
        commit();
    }

    public boolean isUseSwipe() {
        return getDefaultPref().getBoolean("key.useSwipe", true);
    }

    public boolean isExitCheck() {
        return getDefaultPref().getBoolean("key.exitcheck", false);
    }

    public int getAnimationType() {
        return Integer.parseInt(getDefaultPref().getString("key.animationType", "1"));
    }
    
    public boolean isHideAlbumArt(){
        return getDefaultPref().getBoolean("key.hideart", false);
    }
    
    public boolean useLockScreen(){
        return getDefaultPref().getBoolean("key.useLockscreen", true);
    }
    
    public boolean isKeepNotification(){
        return getDefaultPref().getBoolean("key.useKeepNotification", false);
    }
    
    public boolean isKeepPlaybackPosition(){
        return getDefaultPref().getBoolean(SystemConsts.KEY_KEEPPLAYBACKPOS, true);
    }
    
    public boolean useAllCache(){
        return getDefaultPref().getBoolean("key.useAllCache", false);
    }
    
    public boolean useAutoPlay(){
        return getDefaultPref().getBoolean("key.useAutoPlay", true);
    }

    public int getThemeId() {
        return Integer.parseInt(getDefaultPref().getString("key.theme", "0"));
    }
    
    public int getCacheCount(){
        return Funcs.getInt(getDefaultPref().getString("key.cachecount", "3"));
    }
    
    public long getCacheSize(){
        int size =  Funcs.parseInt(getDefaultPref().getString(SystemConsts.KEY_CACHE_SIZE,
                "4"));
        if (size == 0) {
            return 0;
        } else if (size == 1) {
            return 100;
        } else if (size == 2) {
            return 200;
        } else if (size == 3) {
            return 500;
        } else if (size == 4) {
            return 1024;
        } else if (size == 5) {
            return 2048;
        } else if (size == 6) {
            return 5120;
        } else if (size == 7) {
            return 10240;
        } else if (size == 8) {
            return 20480;
        } else if (size == 9) {
            return -1;
        } else {
            return 2147;
        }
    }

    public String getContentUri() {
        return getDefaultPref().getString(SystemConsts.PREF_CONTENTURI,
                DeviceContentProvider.MEDIA_AUTHORITY);
    }
    
    public long getHideTime() {
        int type = Integer.parseInt(getDefaultPref().getString("key.control_hide", "1"));
        long time = 0L;
        if (type == 1) {
            time = 1000L;
        }
        else if (type == 2) {
            time = 2000L;
        }
        else if (type == 3) {
            time = 5000L;
        }
        else if (type == 4) {
            time = 10000L;
        }
        return time;
    }
    
    public void setResumeReloadFlag(boolean b) {
        Editor editor = getEditor();
        if (b) {
            editor.putBoolean(SystemConsts.PREF_RELAOD, true);
        } else {
            editor.remove(SystemConsts.PREF_RELAOD);
        }
        commit();
    }
    
    public void setEarPhoneChecker(boolean b){
        Editor editor = getEditor();
        editor.putBoolean("earPhoneChecker", b);
        commit();
    }
    
    public boolean useEndOfList() {
        return getDefaultPref().getBoolean("key.useEndOfList", true);
    }
    
    public boolean isAutoVaolumeAjust() {
        return getDefaultPref().getBoolean(SystemConsts.KEY_AUTO_VOLUMEAJUST, true);
    }
    
    public boolean isClickAndBack() {
        return getDefaultPref().getBoolean("key.clickandpopback", false);
    }

    public boolean isResumeReloadFlag() {
        return getDefaultPref().getBoolean(SystemConsts.PREF_RELAOD, false);
    }
    
    public boolean canDownload() {
        if (Build.VERSION.SDK_INT > 8) {
            if (!ContentsUtils.isSDCard(this)) {
                return true;
            }
        }
        return false;
    }
    
    public int getFirstVolume(){
        return getDefaultPref().getInt(SystemConsts.KEY_FIRST_VOLUME, 2);
    }
    
    public void setFirstVolume(int v){
        Editor editor = getEditor();
        editor.putInt(SystemConsts.KEY_FIRST_VOLUME, v);
        commit();
    }
    
    public boolean useControlNotification(){
        return getDefaultPref().getBoolean("key.useControlNotification", true);
    }
    
    public boolean useLastFM(){
        return getDefaultPref().getBoolean("key.useLastFM", false);
    }
    
    public boolean useCacheServer(){
        return getDefaultPref().getBoolean("key.useCacheServer", true);
    }
    
    public boolean useHttpClient(){
        return getDefaultPref().getBoolean("key.useHttpClient", false);
    }
    
    public boolean useOffsetLoading(){
        return getDefaultPref().getBoolean("key.useOffsetLoading", false);
    }
    
    public void setUseLastFM(boolean b){
        Editor editor = getEditor();
        editor.putBoolean("key.useLastFM", b);
        commit();
    }
    
    public int getInteger(String key, int value){
        return getDefaultPref().getInt(key, value);
    }

    public void putInt(String key, int value){
        getEditor().putInt(key, value);
    }
    
    public String getStr(String key, String value){
        return getDefaultPref().getString(key, value);
    }
    
    public void putString(String key, String value){
        getEditor().putString(key, value);
    }
}
