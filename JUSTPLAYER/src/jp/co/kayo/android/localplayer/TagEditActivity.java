package jp.co.kayo.android.localplayer;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.cmc.music.common.ID3WriteException;
import org.cmc.music.metadata.ImageData;
import org.cmc.music.metadata.MusicMetadata;
import org.cmc.music.metadata.MusicMetadataSet;
import org.cmc.music.myid3.MyID3;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.adapter.EditItemAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.dialog.EncodeStringListDialog;
import jp.co.kayo.android.localplayer.dialog.EncodeStringListDialog.Callback;
import jp.co.kayo.android.localplayer.dialog.ProgressFragment;
import jp.co.kayo.android.localplayer.menu.SelectAlbumArtMenu;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerServiceCallback;
import jp.co.kayo.android.localplayer.task.ProgressTask;
import jp.co.kayo.android.localplayer.util.FileUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyID3Dat;
import jp.co.kayo.android.localplayer.util.StrictHelper;
import jp.co.kayo.android.localplayer.util.StringEncode;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.EditItem;
import jp.co.kayo.android.localplayer.util.bean.SelectedItemLoader;
import jp.co.kayo.android.localplayer.util.bean.EditItem.ITEM_TYPE;
import jp.co.kayo.android.localplayer.widget.DetectableKeyboardEventLayout;
import jp.co.kayo.android.localplayer.widget.DetectableKeyboardEventLayout.KeyboardListener;

@SuppressLint("NewApi")
public class TagEditActivity extends BaseActivity implements OnClickListener,
        Callback, LoaderCallbacks<List<EditItem>>, OnItemClickListener,
        OnFocusChangeListener {
    public static final int ALBUM = 0;
    public static final int ARTIST = 1;
    public static final int MEDIA = 2;
    private static final int ALBUMART_SIZE = 256;
    private int editType;
    private int imageKey;
    private long[] editKeys;
    private EditItem selectedItem;

    private ListView mListView;
    private EditItemAdapter mAdapter;

    private String editAlbumArtFile;
    private ImageView imageAlbumArt;
    private EditText editTitle;
    private EditText editArtist;
    private EditText editAlbum;
    private EditText editAlbumArtist;
    private EditText editGenre;
    private EditText editYear;
    private EditText editFirstYear;
    private EditText editLastYear;
    private EditText editComment;
    private EditText editTrack;

    private ViewCache mViewCache;
    private Handler mHandler = new MyHandler(this);
    private boolean isSDCard = true;
    private String encode = null;

    private static class MyHandler extends Handler {
        WeakReference<TagEditActivity> ref;

        MyHandler(TagEditActivity r) {
            ref = new WeakReference<TagEditActivity>(r);
        }

        @Override
        public void handleMessage(Message msg) {
            TagEditActivity main = ref.get();
            if (main != null) {
                if (msg.what == SystemConsts.EVT_SELECT_ALBUMART_WEB) {
                    main.selectAlbumArt(true);
                } else if (msg.what == SystemConsts.EVT_SELECT_ALBUMART_LOCAL) {
                    main.selectAlbumArt(false);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        StrictHelper.registStrictMode();
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);

        hideProgressBar();

        setContentView(R.layout.tagediter_album);

        getSupportActionBar().setSubtitle(getString(R.string.hello));
        getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        isSDCard = ContentsUtils.isSDCard(this);

        mListView = (ListView) findViewById(android.R.id.list);
        mListView.setOnItemClickListener(this);

        imageAlbumArt = (ImageView) findViewById(R.id.imageAlbumArt);
        editTitle = (EditText) findViewById(R.id.editTitle);
        editArtist = (EditText) findViewById(R.id.editArtist);
        editAlbum = (EditText) findViewById(R.id.editAlbum);
        editAlbumArtist = (EditText) findViewById(R.id.editAlbumArtist);
        editGenre = (EditText) findViewById(R.id.editGenre);
        editYear = (EditText) findViewById(R.id.editYear);
        editFirstYear = (EditText) findViewById(R.id.editFirstYear);
        editLastYear = (EditText) findViewById(R.id.editLastYear);
        editComment = (EditText) findViewById(R.id.editComment);
        editTrack = (EditText) findViewById(R.id.editTrack);

        editTitle.setOnFocusChangeListener(this);
        editArtist.setOnFocusChangeListener(this);
        editAlbum.setOnFocusChangeListener(this);
        editAlbumArtist.setOnFocusChangeListener(this);
        editGenre.setOnFocusChangeListener(this);
        editYear.setOnFocusChangeListener(this);
        editFirstYear.setOnFocusChangeListener(this);
        editLastYear.setOnFocusChangeListener(this);
        editComment.setOnFocusChangeListener(this);
        editTrack.setOnFocusChangeListener(this);

        findViewById(R.id.imageAlbumArt).setOnClickListener(this);
        findViewById(R.id.btnApply).setOnClickListener(this);
        findViewById(R.id.btnBack).setOnClickListener(this);

        DetectableKeyboardEventLayout root = (DetectableKeyboardEventLayout) findViewById(R.id.root);
        root.setKeyboardListener(new KeyboardListener() {

            @Override
            public void onKeyboardShown() {
                mListView.setVisibility(View.GONE);
            }

            @Override
            public void onKeyboardHidden() {
                if (mAdapter != null) {
                    int listsize = mAdapter.getCount();
                    if (listsize < 2) {
                        findViewById(android.R.id.list)
                                .setVisibility(View.GONE);
                    } else {
                        findViewById(android.R.id.list).setVisibility(
                                View.VISIBLE);
                    }
                }
            }
        });

        // ViewCache
        mViewCache = (ViewCache) getSupportFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);

        Intent intent = getIntent();
        if (intent != null) {
            editType = intent.getIntExtra(SystemConsts.KEY_EDITTYPE, -1);
            imageKey = intent.getIntExtra(SystemConsts.KEY_IMAGEKEY, 0);
            editKeys = intent.getLongArrayExtra(SystemConsts.KEY_EDITKEYLIST);

        }

        if (editType == ALBUM || editType == ARTIST || editType == MEDIA) {
            setupDispleyEdit(editType);
        } else {
            setResult(RESULT_CANCELED);
            finish();
        }

        getSupportLoaderManager().initLoader(R.layout.tagediter_album, null,
                this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getSupportLoaderManager().destroyLoader(R.layout.tagediter_album);
        deleteCacheFile();
        Bitmap bmp = (Bitmap) imageAlbumArt.getTag();
        if (bmp != null) {
            bmp.recycle();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("onActivityResult:"+requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SystemConsts.REQUEST_ALBUMART_WEB) {
                deleteCacheFile();
                editAlbumArtFile = data.getStringExtra("savefile");
                File file = new File(editAlbumArtFile);
                setAlbumArt(file);
                setCheckEdit(R.id.imageAlbumArt, true);
            } else if (requestCode == SystemConsts.REQUEST_ALBUMART_LOCAL) {
                deleteCacheFile();
                File cacheFile = new File(getCacheDir(), "temp"
                        + System.nanoTime() + ".dat");
                editAlbumArtFile = cacheFile.getAbsolutePath();
                InputStream in = null;
                try {
                    in = getContentResolver().openInputStream(data.getData());
                    FileUtils.copyFile(in, cacheFile);
                    setAlbumArt(cacheFile);
                    setCheckEdit(R.id.imageAlbumArt, true);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e) {
                        }
                    }
                }
            }
        }
    }

    private void deleteCacheFile() {
        if (editAlbumArtFile != null && editAlbumArtFile.length() > 0) {
            File file = new File(editAlbumArtFile);
            if (file.exists()) {
                file.delete();
            }
            editAlbumArtFile = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.edit_menu_items, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu == null) {
            return super.onPrepareOptionsMenu(menu);
        }

        MenuItem mnu_sub_mnu_strenc = menu.findItem(R.id.mnu_sub_mnu_strenc);
        if (Build.VERSION.SDK_INT < 10) {
            mnu_sub_mnu_strenc.setVisible(false);
        }

        MenuItem mnu_select_albumart = menu.findItem(R.id.mnu_select_albumart);
        SelectAlbumArtMenu selectalbumartmenu_provider = (SelectAlbumArtMenu) mnu_select_albumart
                .getActionProvider();
        if (selectalbumartmenu_provider != null) {
            selectalbumartmenu_provider.setHandler(mHandler);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
                break;
            case R.id.mnu_unselect_all: {
                for (int i = 0; i < mAdapter.getCount(); i++) {
                    EditItem o = mAdapter.getItem(i);
                    o.checked = false;
                }
                mAdapter.notifyDataSetChanged();
            }
                break;
            case R.id.mnu_select_all: {
                for (int i = 0; i < mAdapter.getCount(); i++) {
                    EditItem o = mAdapter.getItem(i);
                    o.checked = true;
                }
                mAdapter.notifyDataSetChanged();
            }
                break;
            case R.id.mnu_sub_mnu_strenc: {
                if (selectedItem != null) {
                    String fname = selectedItem.data;
                    if (fname != null && new File(fname).exists()) {
                        EncodeStringListDialog dlg = new EncodeStringListDialog();
                        Bundle args = new Bundle();
                        args.putString(SystemConsts.KEY_SOURCEFILE, fname);
                        dlg.setArguments(args);
                        dlg.setCallback(this);
                        dlg.show(getSupportFragmentManager(), "encstring.dlg");
                    } else {
                        // オリジナルのファイルがないので修正しようがない
                    }
                }

            }
                break;
            }
            return super.onOptionsItemSelected(item);
        } finally {
        }
    }

    private void setCheckedState() {
        setCheckEdit(R.id.imageAlbumArt, editAlbumArtFile!=null);
        setCheckEdit(R.id.editTitle, false);
        setCheckEdit(R.id.editArtist, false);
        setCheckEdit(R.id.editAlbum, false);
        setCheckEdit(R.id.editAlbumArtist, false);
        setCheckEdit(R.id.editGenre, false);
        setCheckEdit(R.id.editYear, false);
        setCheckEdit(R.id.editFirstYear, false);
        setCheckEdit(R.id.editLastYear, false);
        setCheckEdit(R.id.editComment, false);
        setCheckEdit(R.id.editTrack, false);
    }

    private void selectAlbumArt(boolean useWeb) {
        if (useWeb) {
            Intent intent = new Intent(this, AlbumartActivity.class);
            if (Funcs.isNotEmpty(editAlbum.getText().toString()))
                intent.putExtra("album", editAlbum.getText().toString());
            if (Funcs.isNotEmpty(editArtist.getText().toString()))
                intent.putExtra("artist", editArtist.getText().toString());
            if (Funcs.isNotEmpty(editTitle.getText().toString()))
                intent.putExtra("title", editTitle.getText().toString());
            startActivityForResult(intent, SystemConsts.REQUEST_ALBUMART_WEB);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            startActivityForResult(intent, SystemConsts.REQUEST_ALBUMART_LOCAL);
        }
    }

    private boolean isEnableEdit(int id) {
        CheckBox checkbox = null;

        if (id == R.id.imageAlbumArt
                && findViewById(R.id.relativeLayoutAlbumArt).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkAlbumArt);
        } else if (id == R.id.editTitle
                && findViewById(R.id.relativeLayoutTitle).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkTitle);
        } else if (id == R.id.editArtist
                && findViewById(R.id.relativeLayoutArtist).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkArtist);
        } else if (id == R.id.editAlbum
                && findViewById(R.id.relativeLayoutAlbum).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkAlbum);
        } else if (id == R.id.editAlbumArtist
                && findViewById(R.id.relativeLayoutAlbumArtist).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkAlbumArtist);
        } else if (id == R.id.editGenre
                && findViewById(R.id.relativeLayoutGenre).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkGenre);
        } else if (id == R.id.editYear
                && findViewById(R.id.relativeLayoutYear).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkYear);
        } else if (id == R.id.editFirstYear
                && findViewById(R.id.relativeLayoutFirstYear).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkFirstYear);
        } else if (id == R.id.editLastYear
                && findViewById(R.id.relativeLayoutLastYear).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkLastYear);
        } else if (id == R.id.editComment
                && findViewById(R.id.relativeLayoutComment).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkComment);
        } else if (id == R.id.editTrack
                && findViewById(R.id.relativeLayoutTrack).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkTrack);
        }

        if (checkbox != null) {
            return checkbox.isChecked();
        }

        return false;
    }

    private void setCheckEdit(int id, boolean b) {
        CheckBox checkbox = null;

        if (id == R.id.imageAlbumArt
                && findViewById(R.id.relativeLayoutAlbumArt).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkAlbumArt);
        } else if (id == R.id.editTitle
                && findViewById(R.id.relativeLayoutTitle).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkTitle);
        } else if (id == R.id.editArtist
                && findViewById(R.id.relativeLayoutArtist).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkArtist);
        } else if (id == R.id.editAlbum
                && findViewById(R.id.relativeLayoutAlbum).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkAlbum);
        } else if (id == R.id.editAlbumArtist
                && findViewById(R.id.relativeLayoutAlbumArtist).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkAlbumArtist);
        } else if (id == R.id.editGenre
                && findViewById(R.id.relativeLayoutGenre).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkGenre);
        } else if (id == R.id.editYear
                && findViewById(R.id.relativeLayoutYear).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkYear);
        } else if (id == R.id.editFirstYear
                && findViewById(R.id.relativeLayoutFirstYear).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkFirstYear);
        } else if (id == R.id.editLastYear
                && findViewById(R.id.relativeLayoutLastYear).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkLastYear);
        } else if (id == R.id.editComment
                && findViewById(R.id.relativeLayoutComment).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkComment);
        } else if (id == R.id.editTrack
                && findViewById(R.id.relativeLayoutTrack).getVisibility() == View.VISIBLE) {
            checkbox = (CheckBox) findViewById(R.id.checkTrack);
        }

        if (checkbox != null) {
            checkbox.setChecked(b);
        }
    }

    private void setDisableEdit(int id) {
        View group = null;

        if (id == R.id.imageAlbumArt) {
            group = findViewById(R.id.relativeLayoutAlbumArt);
        } else if (id == R.id.editTitle) {
            group = findViewById(R.id.relativeLayoutTitle);
        } else if (id == R.id.editArtist) {
            group = findViewById(R.id.relativeLayoutArtist);
        } else if (id == R.id.editAlbum) {
            group = findViewById(R.id.relativeLayoutAlbum);
        } else if (id == R.id.editAlbumArtist) {
            group = findViewById(R.id.relativeLayoutAlbumArtist);
        } else if (id == R.id.editGenre) {
            group = findViewById(R.id.relativeLayoutGenre);
        } else if (id == R.id.editYear) {
            group = findViewById(R.id.relativeLayoutYear);
        } else if (id == R.id.editFirstYear) {
            group = findViewById(R.id.relativeLayoutFirstYear);
        } else if (id == R.id.editLastYear) {
            group = findViewById(R.id.relativeLayoutLastYear);
        } else if (id == R.id.editComment) {
            group = findViewById(R.id.relativeLayoutComment);
        } else if (id == R.id.editTrack) {
            group = findViewById(R.id.relativeLayoutTrack);
        }

        if (group != null) {
            group.setVisibility(View.GONE);
        }
    }

    private void setupDispleyEdit(int type) {
        if (isSDCard) {
            setDisableEdit(R.id.editAlbumArtist);
            setDisableEdit(R.id.editFirstYear);
            setDisableEdit(R.id.editLastYear);
        } else {
            if (editType == ALBUM) {
                setDisableEdit(R.id.editTitle);
                setDisableEdit(R.id.editAlbumArtist);
                setDisableEdit(R.id.editGenre);
                setDisableEdit(R.id.editYear);
                setDisableEdit(R.id.editComment);
                setDisableEdit(R.id.editTrack);
            } else if (editType == ARTIST) {
                setDisableEdit(R.id.editTitle);
                setDisableEdit(R.id.editAlbum);
                setDisableEdit(R.id.editAlbumArtist);
                setDisableEdit(R.id.editGenre);
                setDisableEdit(R.id.editYear);
                setDisableEdit(R.id.editFirstYear);
                setDisableEdit(R.id.editLastYear);
                setDisableEdit(R.id.editComment);
                setDisableEdit(R.id.editTrack);
            } else if (editType == MEDIA) {
                setDisableEdit(R.id.editComment);
                setDisableEdit(R.id.editFirstYear);
                setDisableEdit(R.id.editLastYear);
            }
        }
    }

    private void resetDisplayItem(int pos) {
        Logger.d("resetDisplayItem:"+pos);
        int listsize = mAdapter.getCount();
        if (listsize < 2) {
            findViewById(android.R.id.list).setVisibility(View.GONE);
        } else {
            findViewById(android.R.id.list).setVisibility(View.VISIBLE);
        }
        if (listsize > pos) {
            for (int i = 0; i < mAdapter.getCount(); i++) {
                EditItem o = mAdapter.getItem(i);
                o.checked = false;
            }
            selectedItem = mAdapter.getItem(pos);
            selectedItem.checked = true;
            if (isSDCard) {
                HashMap<String, String> map = new HashMap<String, String>();
                Bitmap bitmap = Funcs.loadBitmapFromMedia(
                        new File(selectedItem.data),
                        ALBUMART_SIZE, map);
                editTitle.setText(trimString(map.get("title")));
                editAlbum.setText(trimString(map.get("album")));
                editArtist.setText(trimString(map.get("artist")));
                editYear.setText(trimString(map.get("year")));
                // editAlbumArtist.setText(arrayToString(meta.getFeaturingList()));
                editTrack.setText(trimString(map.get("track")));
                editGenre.setText(trimString(map.get("genre")));
                editComment.setText(trimString(map.get("comment")));
                if(editAlbumArtFile == null){
                    setBitmap(bitmap);
                }else if(bitmap!=null){
                    bitmap.recycle();
                }
            } else {
                if (selectedItem.type == EditItem.ITEM_TYPE.ALBUM) {
                    editAlbum.setText(trimString(selectedItem.album));
                    editArtist.setText(trimString(selectedItem.artist));
                    editFirstYear.setText(Integer
                            .toString(selectedItem.firstyear));
                    editLastYear.setText(Integer
                            .toString(selectedItem.lastyear));
                    if(editAlbumArtFile == null){
                        setAlbumArt(selectedItem);
                    }
                } else if (selectedItem.type == EditItem.ITEM_TYPE.ARTIST) {
                    editArtist.setText(trimString(selectedItem.artist));
                    if(editAlbumArtFile == null){
                        setAlbumArt(selectedItem);
                    }
                } else if (selectedItem.type == EditItem.ITEM_TYPE.SONG) {
                    editTitle.setText(trimString(selectedItem.title));
                    editAlbum.setText(trimString(selectedItem.album));
                    editArtist.setText(trimString(selectedItem.artist));
                    editYear.setText(Integer.toString(selectedItem.year));
                    editTrack.setText(Integer.toString(selectedItem.track));
                    if(editAlbumArtFile == null){
                        setAlbumArt(selectedItem);
                    }
                }
            }
        }
        setCheckedState();
        mAdapter.notifyDataSetChanged();
    }

    private String trimString(String s) {
        if (s != null) {
            return s.trim();
        }
        return "";
    }

    private Integer parseInteger(String s) {
        if (s != null && s.length() > 0) {
            try{
                return new Integer(s);
            }catch(Exception e){
            }
        }
        return new Integer(0);
    }

    private void setAlbumArt(EditItem item) {
        File file = null;
        Bitmap bitmap = null;
        if (item.type == ITEM_TYPE.ALBUM) {
            Integer key = Funcs.getAlbumKey(item.album, item.artist);
            file = ViewCache.createAlbumArtFile(key);
            if (!file.exists() && Funcs.isNotEmpty(item.art)) {
                File srcFile = new File(item.art);
                if (srcFile.exists()) {
                    try {
                        FileUtils.copyFile(srcFile, file);
                    } catch (IOException e) {
                    }
                }
            }
        } else if (item.type == ITEM_TYPE.ARTIST) {
            Integer key = Funcs.getAlbumKey(null, item.artist);
            file = ViewCache.createAlbumArtFile(key);
        }
        if (item.type == ITEM_TYPE.SONG) {
            if (Build.VERSION.SDK_INT >= 10) {
                try {
                    String fname = item.data;
                    if(fname!=null){
                        File cacheFile = new File(fname);
                        if (cacheFile != null && cacheFile.exists()) {
                            bitmap = Funcs.loadBitmapFromMedia(cacheFile,
                                    ALBUMART_SIZE);
                        }
                    }
                } catch (Exception e) {
                    Logger.e("getEmbeddedPicture", e);
                }
            }
        }

        if (bitmap != null) {
            setBitmap(bitmap);
        } else {
            setAlbumArt(file);
        }
    }

    private void setAlbumArt(File file) {
        Bitmap bitmap = null;
        if (file != null && file.exists()) {
            bitmap = Funcs.loadBitmap(file, ALBUMART_SIZE);
        }
        setBitmap(bitmap);
    }

    private void setBitmap(Bitmap bitmap) {
        Bitmap bmp = (Bitmap) imageAlbumArt.getTag();
        if (bmp != null) {
            bmp.recycle();
            imageAlbumArt.setTag(null);
        }
        if (bitmap != null) {
            imageAlbumArt.setImageBitmap(bitmap);
            imageAlbumArt.setTag(bitmap);
            return;
        }
        imageAlbumArt.setImageResource(R.drawable.albumart_mp_unknown);
    }

    @Override
    IMediaPlayerServiceCallback getCallBack() {
        return null;
    }

    @Override
    ViewCache getViewCache() {
        return mViewCache;
    }

    @Override
    Handler getHandler() {
        return mHandler;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnBack) {
            setResult(RESULT_OK);
            finish();
        } else if (v.getId() == R.id.btnApply) {
            // 適用して終了
            apply();
        } else if (v.getId() == R.id.imageAlbumArt) {
            ArrayList<String> menuItems = new ArrayList<String>();
            menuItems.add(getString(R.string.sub_mnu_albumart_search));
            menuItems.add(getString(R.string.sub_mnu_albumart_local));

            final CharSequence[] items = menuItems
                    .toArray(new CharSequence[menuItems.size()]);
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.mnu_select_albumart))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            if (item == 0) {
                                mHandler.sendMessage(mHandler.obtainMessage(
                                        SystemConsts.EVT_SELECT_ALBUMART_WEB,
                                        Integer.valueOf(1)));
                            } else if (item == 1) {
                                mHandler.sendMessage(mHandler.obtainMessage(
                                        SystemConsts.EVT_SELECT_ALBUMART_LOCAL,
                                        Integer.valueOf(2)));
                            }
                        }
                    }).show();
        }

    }

    @Override
    public void onEncodedStringSelect(String s, String enc, String srcFile) {
        try {
            encode = enc;
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(this, Uri.parse(srcFile));
            String album = mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            String artist = mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            String title = mmr
                    .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            String comment = mmr.extractMetadata(14);

            if (title != null) {
                String new_title = StringEncode.getEncodeString(this, title,
                        enc);
                if (!new_title.equals(editTitle.getText().toString())) {
                    editTitle.setText(new_title);
                    selectedItem.title = new_title;
                }
            }
            if (artist != null) {
                String new_artist = StringEncode.getEncodeString(this, artist,
                        enc);
                if (!new_artist.equals(editArtist.getText().toString())) {
                    editArtist.setText(new_artist);
                    selectedItem.artist = new_artist;
                }
            }

            if (album != null) {
                String new_album = StringEncode.getEncodeString(this, album,
                        enc);
                if (!new_album.equals(editAlbum.getText().toString())) {
                    editAlbum.setText(new_album);
                    selectedItem.album = new_album;
                }
            }

            if (comment != null) {
                String new_comment = StringEncode.getEncodeString(this,
                        comment, enc);
                if (!new_comment.equals(editComment.getText().toString())) {
                    editComment.setText(new_comment);
                    selectedItem.comment = new_comment;
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    private boolean hasSelectedItems() {
        for (int i = 0; i < mAdapter.getCount(); i++) {
            EditItem item = mAdapter.getItem(i);
            if (item.checked) {
                return true;
            }
        }

        return false;
    }

    private void apply() {
        ProgressTask task = new ProgressTask(getSupportFragmentManager()) {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    if (isSDCard) {
                        SDCardDataApply(this.dialog);
                    } else {
                        nonSDCardDataApply(this.dialog);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                mAdapter.notifyDataSetChanged();
            }
        };
        task.execute();
    }

    private void SDCardDataApply(ProgressFragment dialog) throws IOException {
        // 直接データの変更ができないので、MP3タグを修正する。そのあと再読込を促す
        // TODO MP3だけタグ編集が可能
        ArrayList<String> files = new ArrayList<String>();
        if (hasSelectedItems()) {
            MyID3 myid3 = new MyID3();
            Vector<ImageData> fileList = new Vector<ImageData>();
            Vector<ImageData> fileListByMedia = new Vector<ImageData>();
            if (editAlbumArtFile != null && editAlbumArtFile.length() > 0) {
                File file = new File(editAlbumArtFile);
                if (file.exists()) {
                    ImageData data = new ImageData(Funcs.readFile(file), "",
                            "", 3);
                    fileList.add(data);
                }
            } else if (selectedItem != null) {
                MusicMetadataSet myset;
                try {
                    myset = myid3
                            .read(new File(selectedItem.data));
                    if (myset != null) {
                        MusicMetadata meta = (MusicMetadata) myset
                                .getSimplified();
                        fileListByMedia = meta.getPictureList();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (Build.VERSION.SDK_INT > 9 && fileListByMedia.size() == 0) {
                    MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                    mmr.setDataSource(selectedItem.data);
                    ImageData data = new ImageData(mmr.getEmbeddedPicture(),
                            "", "", 3);
                    fileListByMedia.add(data);
                }
            }

            dialog.setMax(mAdapter.getCount());
            for (int i = 0; i < mAdapter.getCount(); i++) {
                EditItem item = mAdapter.getItem(i);
                if (item.checked) {
                    File srcFile = new File(item.data);
                    if (srcFile.exists()) {
                        MediaMetadataRetriever mmr = null;
                        if (encode != null) {
                            mmr = new MediaMetadataRetriever();
                            mmr.setDataSource(srcFile.getAbsolutePath());
                        }
                        try {
                            MusicMetadataSet src_set = myid3.read(srcFile);
                            if (src_set != null) {
                                MusicMetadata metadata = (MusicMetadata) src_set
                                        .getSimplified();
                                boolean dirty = false;
                                if (isEnableEdit(R.id.editTitle)) {
                                    metadata.setSongTitle(editTitle.getText()
                                            .toString());
                                    item.title = editTitle.getText().toString();
                                    dirty = true;
                                } else if (mmr != null) {
                                    String source = mmr
                                            .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                                    if (source != null) {
                                        String enctext = StringEncode
                                                .getEncodeString(this, source,
                                                        encode);
                                        metadata.setSongTitle(enctext);
                                        item.title = enctext;
                                        dirty = true;
                                    }
                                }
                                if (isEnableEdit(R.id.editArtist)) {
                                    metadata.setArtist(editArtist.getText()
                                            .toString());
                                    item.artist = editArtist.getText()
                                            .toString();
                                    dirty = true;
                                } else if (encode != null) {
                                    String source = mmr
                                            .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                                    if (source != null) {
                                        String enctext = StringEncode
                                                .getEncodeString(this, source,
                                                        encode);
                                        metadata.setArtist(enctext);
                                        item.artist = enctext;
                                        dirty = true;
                                    }
                                }
                                if (isEnableEdit(R.id.editAlbum)) {
                                    metadata.setAlbum(editAlbum.getText()
                                            .toString());
                                    item.album = editAlbum.getText().toString();
                                    dirty = true;
                                } else if (encode != null) {
                                    String source = mmr
                                            .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
                                    if (source != null) {
                                        String enctext = StringEncode
                                                .getEncodeString(this, source,
                                                        encode);
                                        metadata.setAlbum(enctext);
                                        item.album = enctext;
                                        dirty = true;
                                    }
                                }
                                if (isEnableEdit(R.id.editComment)) {
                                    metadata.setComment(editComment.getText()
                                            .toString());
                                    item.comment = editComment.getText()
                                            .toString();
                                    dirty = true;
                                } else if (encode != null) {
                                    String source = mmr.extractMetadata(14);
                                    if (source != null) {
                                        String enctext = StringEncode
                                                .getEncodeString(this, source,
                                                        encode);
                                        metadata.setComment(enctext);
                                        item.comment = enctext;
                                        dirty = true;
                                    }
                                }
                                if (isEnableEdit(R.id.editYear)) {
                                    metadata.setYear(editYear.getText()
                                            .toString());
                                    item.year = parseInteger(editYear.getText().toString());
                                    dirty = true;
                                }
                                if (isEnableEdit(R.id.editTrack)) {
                                    metadata.setTrackNumber(parseInteger(editTrack
                                            .getText().toString()));
                                    item.track = parseInteger(editTrack.getText().toString());
                                    dirty = true;
                                }
                                if (isEnableEdit(R.id.imageAlbumArt)) {
                                    if (fileList.size() > 0) {
                                        metadata.setPictureList(fileList);
                                        dirty = true;
                                    } else if (fileListByMedia.size() > 0) {
                                        if (item.id != selectedItem.id) {
                                            metadata.setPictureList(fileListByMedia);
                                            dirty = true;
                                        }
                                    }
                                }

                                if (dirty) {
                                    myid3.update(srcFile, src_set,
                                            (MusicMetadata) metadata);
                                    files.add(srcFile.toString());
                                }
                            }
                        } catch (IOException e1) {
                            // MP3じゃないので更新できない
                            e1.printStackTrace();
                        } catch (ID3WriteException e) {
                            // MP3じゃないので更新できない
                            e.printStackTrace();
                        }

                    }
                }
                dialog.setProgress(i);
            }

            if (Build.VERSION.SDK_INT > 7) {
                MediaScannerConnection.scanFile(this,
                        (String[]) files.toArray(new String[files.size()]),
                        null, null);
            }
        }

        // AlbumArt(Album, Artist)
        if (isEnableEdit(R.id.imageAlbumArt)) {
            if (editAlbumArtFile != null && editAlbumArtFile.length() > 0
                    && imageKey != 0) {
                File srcfile = new File(editAlbumArtFile);
                File desFile = ViewCache.createAlbumArtFile(imageKey);
                if (desFile.exists()) {
                    desFile.delete();
                }
                FileUtils.copyFile(srcfile, desFile);
            } else if (imageKey != 0 && selectedItem != null
                    && Funcs.isNotEmpty(selectedItem.data)) {
                Bitmap bitmap = Funcs.loadBitmapFromMedia(
                        new File(selectedItem.data), 512);
                if (bitmap != null) {
                    File desFile = ViewCache.createAlbumArtFile(imageKey);
                    FileOutputStream outStream = null;
                    try {
                        outStream = new FileOutputStream(desFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100,
                                outStream);
                        outStream.flush();
                        outStream.close();
                    } finally {
                        if (outStream != null) {
                            outStream.close();
                        }
                    }
                }
            }
        }
    }

    private void nonSDCardDataApply(ProgressFragment dialog) throws IOException {
        if (hasSelectedItems()) {
            MyID3Dat myid3 = new MyID3Dat();
            Vector<ImageData> fileList = new Vector<ImageData>();
            Vector<ImageData> fileListByMedia = new Vector<ImageData>();
            if(editType == MEDIA){
                if (editAlbumArtFile != null && editAlbumArtFile.length() > 0) {
                    File file = new File(editAlbumArtFile);
                    if (file.exists()) {
                        ImageData data = new ImageData(Funcs.readFile(file), "",
                                "", 3);
                        fileList.add(data);
                    }
                } else if (selectedItem != null) {
                    MusicMetadataSet myset;
                    try {
                        myset = myid3
                                .read(new File(selectedItem.data));
                        if (myset != null) {
                            MusicMetadata meta = (MusicMetadata) myset
                                    .getSimplified();
                            fileListByMedia = meta.getPictureList();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
    
                    if (Build.VERSION.SDK_INT > 9 && fileListByMedia.size() == 0) {
                        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                        mmr.setDataSource(selectedItem.data);
                        ImageData data = new ImageData(mmr.getEmbeddedPicture(),
                                "", "", 3);
                        fileListByMedia.add(data);
                    }
                }
            }

            dialog.setMax(mAdapter.getCount());
            for (int i = 0; i < mAdapter.getCount(); i++) {
                EditItem item = mAdapter.getItem(i);
                if (item.checked) {
                    if (item.type == ITEM_TYPE.ALBUM) {
                        if (item.id >= 0) {
                            String album = item.album;
                            String artist = item.artist;
                            ContentValues value = new ContentValues();
                            if (isEnableEdit(R.id.editAlbum)) {
                                value.put(AudioAlbum.ALBUM, editAlbum.getText()
                                        .toString());
                                album = editAlbum.getText().toString();
                            }
                            if (isEnableEdit(R.id.editArtist)) {
                                value.put(AudioAlbum.ARTIST, editArtist
                                        .getText().toString());
                                artist = editArtist.getText().toString();
                            }
                            if (isEnableEdit(R.id.editFirstYear))
                                value.put(AudioAlbum.FIRST_YEAR, editFirstYear
                                        .getText().toString());
                            if (isEnableEdit(R.id.editLastYear))
                                value.put(AudioAlbum.LAST_YEAR, editLastYear
                                        .getText().toString());
                            // AlbumArt
                            if (isEnableEdit(R.id.imageAlbumArt)) {
                                if (editAlbumArtFile != null
                                        && editAlbumArtFile.length() > 0) {
                                    File srcfile = new File(editAlbumArtFile);
                                    File desFile = ViewCache
                                            .createAlbumArtFile(Funcs
                                                    .getAlbumKey(album, artist));
                                    if (desFile.exists()) {
                                        desFile.delete();
                                    }
                                    FileUtils.copyFile(srcfile, desFile);
                                    value.put(AudioAlbum.ALBUM_ART,
                                            desFile.toString());
                                }
                            }
                            if (value.size() > 0) {
                                getContentResolver().update(
                                        ContentUris.withAppendedId(
                                                MediaConsts.ALBUM_CONTENT_URI,
                                                item.id), value, null, null);
                            }
                        }
                    } else if (item.type == ITEM_TYPE.ARTIST) {
                        if (item.id >= 0) {
                            String artistKey = item.artistKey;
                            ContentValues value = new ContentValues();
                            if (isEnableEdit(R.id.editArtist))
                                value.put(AudioArtist.ARTIST, editArtist
                                        .getText().toString());
                            if (value.size() > 0) {
                                getContentResolver().update(
                                        ContentUris.withAppendedId(
                                                MediaConsts.ARTIST_CONTENT_URI,
                                                item.id), value, null, null);
                            }
                            // AlbumArt
                            if (isEnableEdit(R.id.imageAlbumArt)) {
                                if (editAlbumArtFile != null
                                        && editAlbumArtFile.length() > 0) {
                                    File srcfile = new File(editAlbumArtFile);
                                    File desFile = ViewCache
                                            .createAlbumArtFile(Funcs
                                                    .getAlbumKey(null,
                                                            artistKey));
                                    if (desFile.exists()) {
                                        desFile.delete();
                                    }
                                    FileUtils.copyFile(srcfile, desFile);
                                }
                            }
                        }
                    } else if (item.type == ITEM_TYPE.SONG) {
                        ContentValues value = new ContentValues();
                        if (isEnableEdit(R.id.editTitle))
                            value.put(AudioMedia.TITLE, editTitle.getText()
                                    .toString());
                        if (isEnableEdit(R.id.editAlbum))
                            value.put(AudioMedia.ALBUM, editAlbum.getText()
                                    .toString());
                        if (isEnableEdit(R.id.editArtist))
                            value.put(AudioMedia.ARTIST, editArtist.getText()
                                    .toString());
                        if (isEnableEdit(R.id.editYear))
                            value.put(AudioMedia.YEAR, editYear.getText()
                                    .toString());
                        if (isEnableEdit(R.id.editTrack))
                            value.put(AudioMedia.TRACK, editTrack.getText()
                                    .toString());
                        // AlbumArt
                        if (isEnableEdit(R.id.imageAlbumArt)) {
                            File srcFile = new File(item.data);
                            if (srcFile.exists()
                                    && (fileList.size() > 0 || fileListByMedia
                                            .size() > 0)) {
                                MusicMetadataSet src_set = myid3.read(srcFile);
                                if (src_set != null) {
                                    MusicMetadata metadata = (MusicMetadata) src_set
                                            .getSimplified();
                                    if (fileList.size() > 0) {
                                        metadata.setPictureList(fileList);
                                        try {
                                            myid3.update(srcFile, src_set,
                                                    (MusicMetadata) metadata);
                                        } catch (ID3WriteException e) {
                                            e.printStackTrace();
                                        }
                                    } else if (fileListByMedia.size() > 0) {
                                        if (item.id != selectedItem.id) {
                                            metadata.setPictureList(fileListByMedia);
                                            try {
                                                myid3.update(
                                                        srcFile,
                                                        src_set,
                                                        (MusicMetadata) metadata);
                                            } catch (ID3WriteException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    }
                                }
                            }
                        }
                        if (value.size() > 0) {
                            getContentResolver().update(
                                    ContentUris.withAppendedId(
                                            MediaConsts.MEDIA_CONTENT_URI,
                                            item.id), value, null, null);
                        }
                    }
                }
                dialog.setProgress(i);
            }
        }
    }

    @Override
    public void hideMenu() {
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
            long arg3) {
        deleteCacheFile();
        resetDisplayItem(position);
    }

    @Override
    public Loader<List<EditItem>> onCreateLoader(int arg0, Bundle arg1) {
        Logger.d("onCreateLoader");
        return new SelectedItemLoader(this, editType, editKeys);
    }

    @Override
    public void onLoadFinished(Loader<List<EditItem>> arg0, List<EditItem> data) {
        mAdapter = new EditItemAdapter(this, data);
        mListView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        if (data != null && data.size() > 0) {
            resetDisplayItem(0);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<EditItem>> data) {
        mAdapter = new EditItemAdapter(this, new ArrayList<EditItem>());
        mListView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFocusChange(View arg0, boolean arg1) {
        if (arg1 == true) {
            setCheckEdit(arg0.getId(), true);
        }

    }
}
