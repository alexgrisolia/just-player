package jp.co.kayo.android.localplayer.dialog;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ProgressFragment extends DialogFragment {
    Handler handler = new Handler();
    ProgressBar progressBar01;
    TextView textView1;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.loading_progress_dialog,
                null);
        builder.setView(view);
        builder.setCancelable(false);
        progressBar01 = (ProgressBar) view.findViewById(R.id.progressBar01);
        textView1 = (TextView)view.findViewById(R.id.textView1);

        return builder.create();
    }

    public void setMax(final int max) {
        handler.post(new Runnable() {

            @Override
            public void run() {
                progressBar01.setMax(max);
            }
        });
    }

    public void setProgress(final int progress) {
        handler.post(new Runnable() {

            @Override
            public void run() {
                progressBar01.setProgress(progress);
            }
        });
    }
    
    public void setText(final String s){
        handler.post(new Runnable() {

            @Override
            public void run() {
                textView1.setText(s);
            }
        });
    }
}
